import cv2 
import numpy as np
import matplotlib.pyplot as plt
import serial

#ser = serial.Serial(port='COM13', baudrate=115200)

frame_width = 640
frame_height = 480


Kp = 1.5
Ki = 0
Kd = 0
Isum = 0
prev_error = 0
setpoint = frame_width/2
offset = 139
left_steering = 99
right_steering = 179
max_throttle = 200
min_throttle = 179

def PID(Kp, Ki, Kd, Isum, setpoint, current,prev_error, offset):
   
    error = setpoint - current
        
    P = Kp*error
    I = Isum + Ki*error
    D = Kd*(error - prev_error)

    prev_error = error

    output = offset + P + I + D
    if(output > right_steering):
      output = 179
    elif(output <left_steering):
      output = 99
    print('PWM:' + str(output))
    output_ser = "S" + str(output)
    #ser.write(output_ser.encode())
    return output



def ROI(frame):
    height = frame.shape[0]
    polygons = np.array([[(0, height), (640, height),(590, 180), (50, 180)]])
    roi = np.zeros_like(frame)
    cv2.fillPoly(roi, polygons, 255)
    return roi

def display_linesP(image, lines):
    line_image = image

    leftx_array = []
    lefty_array = []

    rightx_array = []
    righty_array = []

    if lines is not None:
        for line in lines:
            for x1,y1, x2,y2 in line:
                slope = (y2 - y1) / (x2 - x1)
                
                if(slope > 0 and abs(slope) > 0.7):
                    #print('Right' + str(slope))
                    rightx_array.append(x1)
                    rightx_array.append(x2)
                    righty_array.append(y1)
                    righty_array.append(y2)
                    line_image = cv2.line(image, (x1, y1), (x2, y2), (255, 0, 0), 4)
                elif(abs(slope) > 0.7) :
                    #print('Left' + str(slope))
                    leftx_array.append(x1)
                    leftx_array.append(x2)
                    lefty_array.append(y1)
                    lefty_array.append(y2)
                    line_image = cv2.line(image, (x1, y1), (x2, y2), (0, 0, 255), 4)
               
    #Add ROI
    #cv2.line(line_image, (440, 200), (200, 200), (0, 255, 0) , 2)
    #cv2.line(line_image, (100, 480), ((200, 200)), (0, 255, 0) , 2)
    #cv2.line(line_image, (100, 480), ((200, 200)), (0, 255, 0) , 2)
    #print(len(leftx_array))
  
    #print(len(lefty_array))

    coeff = np.polyfit(leftx_array,lefty_array, deg = 1)
    a1, b1 = coeff
   
    y1 = 180
    y2 = 400
    x1 = int((y1 - coeff[1])/coeff[0])
    x2 = int((y2 - coeff[1])/coeff[0])
    line_image = cv2.line(image, (x1, y1), (x2, y2), (85, 246, 38), 10)
    
    #Right
    coeff = np.polyfit(rightx_array,righty_array, deg = 1)
    a2, b2 = coeff
    y1 = 180
    y2 = 400
    x1 = int((y1 - coeff[1])/coeff[0])
    x2 = int((y2 - coeff[1])/coeff[0])
    line_image = cv2.line(image, (x1, y1), (x2, y2), (85, 246, 38), 10)

    #print('left: ' + str(a1) + ' ' + str(b1))
    #print('right: ' + str(a2) + ' ' + str(b2))
    a = np.array([[0, 1], [a1, 1]])
    b = np.array([300, b1])
    left_point = np.linalg.solve(a, b)
    a = np.array([[0, 1], [a2, 1]])
    b = np.array([300, b2])
    right_point = np.linalg.solve(a, b)
  
    

    #Boundaries for PID
    line_image = cv2.line(line_image, (200, 300), (50, 300), (0, 0, 0) , 20)
    line_image = cv2.line(line_image, (590, 300), (440, 300), (0, 0, 0) , 20)
    

    line_image = cv2.circle(line_image,(int(abs(left_point[0])) , 300), radius=0, color=(0, 0, 255), thickness=15)
    line_image = cv2.circle(line_image,(int(abs(right_point[0])) , 300), radius=0, color=(255, 0, 0), thickness=15)

    left_pos = int(abs(left_point[0]))
    right_pos = int(abs(right_point[0]))

    mid = int(left_pos + (right_pos-left_pos)/2)

    
    out = PID(Kp, Ki, Kd, Isum, setpoint, mid,prev_error, offset)


    font = cv2.FONT_HERSHEY_SIMPLEX 
  

    org = (100, 50) 
    org1 = (100, 100)
  
    fontScale = 0.8
    color = (255,255,255) 
    thickness = 2
   

    line_image= cv2.putText(line_image, 'PID values: ' + 'Kp:' +str(Kp) + ' Ki:' +str(Ki)+ ' Kd:' +str(Kd), org, font,fontScale, color, thickness, cv2.LINE_AA) 
    line_image= cv2.putText(line_image, 'PWM OUPUT: '  +str(out), org1, font,fontScale, color, thickness, cv2.LINE_AA) 

    print(Ki)

    line_image = cv2.circle(line_image,( mid , 300), radius=0, color=(255, 255, 0), thickness=15)

    #print(mid)

    #line_image = cv2.line(line_image, (640/2, 300), (640/2, 500), (0, 0, 0) , 20)

    #line_image = cv2.line(line_image, (mid, 300), (mid, 500), (0, 0, 0) , 20)
    return line_image




cap = cv2.VideoCapture('E:\\OneDrive\\Diplomka\\line_detection\\road.mp4')


 
if (cap.isOpened()== False): 
  print("Error opening video stream or file")
 

while(cap.isOpened()):
 
  index =0
  ret, frame = cap.read()
  if ret == True:
 
    resize_image =  cv2.resize(frame, (640, 480))
    colorspace_image = cv2.cvtColor(resize_image, cv2.COLOR_RGB2GRAY) 
    frame_blur = cv2.GaussianBlur(colorspace_image,(13,13),sigmaX=10)

    ret, thresh1 = cv2.threshold(frame_blur, 150, 255, cv2.THRESH_BINARY) 

    frame_edge = cv2.Canny(thresh1, 200, 50)


    frame_roi = ROI(frame_edge)
    frame_bitwise = cv2.bitwise_and(frame_edge, frame_roi)
    img_dilation = cv2.dilate(frame_bitwise, np.ones((3, 3), np.uint8), iterations=3)
    detected_lines = cv2.HoughLinesP(img_dilation, 1, np.pi/180,threshold=80,minLineLength=20)
    frame_final = display_linesP(resize_image,detected_lines) 

    cv2.imshow('Line Detection',frame_final)
 

    if cv2.waitKey(25) & 0xFF == ord('q'):
      break
 
  # Break the loop
  else: 
    break
 

cap.release()
 

cv2.destroyAllWindows()